﻿namespace SurvivalNightStory
{
    public struct Dialog
    {
        public int next;
        public bool buttons;
        public string image;
        public string message;
        public LoadTextOption option;
        public int yes;
        public string textYes;
        public int no;
        public string textNo;
        public bool loop;

        public Dialog(int next, string image, bool loop, string message, LoadTextOption option)
        {
            this.yes = -1;
            this.textYes = "";
            this.no = -1;
            this.textNo = "";
            this.next = next;
            this.buttons = false;
            this.image = image;
            this.message = message;
            this.option = option;
            this.loop = loop;
        }

        public Dialog(int yes, string textYes, int no, string textNo, string image, bool loop, string message)
        {
            this.yes = yes;
            this.textYes = textYes;
            this.no = no;
            this.textNo = textNo;
            this.next = -1;
            this.buttons = true;
            this.image = image;
            this.loop = loop;
            this.message = message;
            this.option = LoadTextOption.EndWithOptions;
        }
    }

    public static class StoryScript
    {
        //TODO:Poner arrobas en todos los textos

        public static Dialog[] dialogs = {
        //1.	La fogata
        new Dialog(1, "image001", true, @"Hace un par de horas que cayó la <b>noche</b>. Ya sólo me quedan un par de latas de atún, " +
            @"una barra de chocolate y algunos restos de la ardilla que capturé en la tarde.", LoadTextOption.EndWithArrow),
        new Dialog(2, "", true, @"Ojalá no hubiese perdido mi mapa; lo admito, " +
            @"fue descuidado de mi parte aventurarme sólo en el bosque.", LoadTextOption.EndWithArrow),
        new Dialog(3, "", true,  @"Si mi intuición no me falla debería estar cerca de la carretera. " +
            @"Mañana levantaré todo desde temprano para salir de este bosque antes del anochecer.", LoadTextOption.EndWithoutArrow),
        new Dialog(4, "image002", true,  @"<i>(<size=25>Se escuchan crujidos </size>a <b>unos cincuenta metros</b> del campamento)</i>", LoadTextOption.EndWithoutArrow),
        new Dialog(5, "Inspeccionar", 9, "No hacer nada", "", true, "¿Vas a ir a inspeccionar?"),
        //2.  La inspección (Respuesta a ‘A’)
        new Dialog(6, "image003", false,  "(Encuentras una nota sucia y arrugada junto a una bota destrozada)", LoadTextOption.EndWithArrow),
        new Dialog(7, "", true,  "¿Qué demonios?", LoadTextOption.EndWithArrow),
        new Dialog(8, "image004", true,  "(Lees la nota:) “Esta es tu última oportunidad, sigue el sendero hacia el alba…”", LoadTextOption.EndWithArrow),
        new Dialog(15, "image005", false,  "(Mientras lees, detrás de ti pasa una imagen obscura de alguien)", LoadTextOption.EndWithoutArrow),
        //3.	El campamento (Respuesta a ‘B’)
        new Dialog(10, "image006", true,  "(Decides quedarte a cenar ignorando aquel ruido en los arbustos)", LoadTextOption.EndWithoutArrow),
        new Dialog(11, "", true,  "Si es un animal salvaje permaneceré alerta.", LoadTextOption.EndWithArrow),
        new Dialog(12, "", true,  "He terminado de cenar, pasaré la noche y al amanecer pondré rumbo a la carretera.", LoadTextOption.EndWithoutArrow),
        new Dialog(13, "image007", false,  "(Estás dormido, pero otro ruido te despierta)", LoadTextOption.EndWithoutArrow),
        new Dialog(14, "image008", false,  "<size=25>¿Qué está pasa...?</size><size=35> ¡Aaagh!</size>", LoadTextOption.EndWithArrow),
        new Dialog(-1, "", true,  "<size=35><color=#ff0000>Fin del juego</color></size>", LoadTextOption.EndWithoutArrow),
        //4.  El camino
        new Dialog(16, "", true,  "¿Quién habrá dejado esa nota ahí? ¿Qué demonios significa esto?", LoadTextOption.EndWithArrow),
        new Dialog(17, "Obedecer la nota", 29, "Seguir tu camino", "", true,
            "<size=18>Será mejor que emprenda camino de una vez. Si espero al amanecer podría terminar muerto… pero, ¿a dónde? " +
            "Puedo seguir hacia la carretera o hacerle caso a la nota e ir camino hacia el alba.</size>"),
        //5.  Camino al alba (Respuesta a ‘A’)
        new Dialog(18, "image009", true,  "Está próximo a amanecer, quizás debí ignorar esa nota. "
            + "¿Qué haré si me pierdo y me quedo sin comida?", LoadTextOption.EndWithoutArrow),
        new Dialog(19, "", true,  "(Se escucha un lamento más adelante)", LoadTextOption.EndWithoutArrow),
        new Dialog(20, "image010", true,  "David, ¿qué haces aquí?", LoadTextOption.EndWithArrow),
        new Dialog(21, "", true,  "–Amigo… que bueno que estás aquí, pensé que moriría – Dice el amigo todo débil junto a un árbol.",
            LoadTextOption.EndWithArrow),
        new Dialog(22, "", true,  "Fue ese maldito loco… logré huir de él, pero recibí una herida profunda en la pierna. ¿Podrías ayudarme?",
            LoadTextOption.EndWithoutArrow),
        new Dialog(23, "image011", false,  "(Levantas a David y lo llevas a cuestas.)", LoadTextOption.EndWithoutArrow),
        new Dialog(24, "", true,  "Ya todo estará bien, David, saldremos de aquí.", LoadTextOption.EndWithArrow),
        new Dialog(25, "", true,  "Sí… todo estará bien – Contesta David.", LoadTextOption.EndWithoutArrow),
        new Dialog(26, "image012", false,  "(Levantas a David para caminar al alba, mientras él muestra un cuchillo en su espalda)", LoadTextOption.EndWithoutArrow),
        new Dialog(27, "", false,  "Todo estará <b>bien</b> – Dice David al último.", LoadTextOption.EndWithArrow),
        new Dialog(-1, "", true,  "<size=35><color=#ff0000>Fin del juego</color></size>", LoadTextOption.EndWithoutArrow),
        //6.	La carretera (Respuesta a ‘B’)
        new Dialog(29, "image013", false,  "Llevo varias horas caminando, no he vuelto a escuchar algún ruido extraño tras de mí. " +
            "Estoy a punto de llegar a la carretera, parece ser que no estaba tan lejos como creí.", LoadTextOption.EndWithArrow),
        new Dialog(30, "", true,  "Se vislumbran las primeras luces del amanecer. " +
            "Probablemente algún conductor pueda llevarme o me regreso a la ciudad.", LoadTextOption.EndWithArrow),
        new Dialog(31, "", true,  "(Llega un vehículo de paquetería y se para con él)", LoadTextOption.EndWithoutArrow),
        new Dialog(32, "", true,  "¡Buenas! Voy camino a la ciudad, ¿podría llevarme con usted?", LoadTextOption.EndWithArrow),
        new Dialog(33, "", true,  "Por supuesto muchacho, sube por la parte de atrás " +
            "– Dijo el conductor, a quien no se le ve bien la cara.", LoadTextOption.EndWithoutArrow),
        new Dialog(34, "imagen14", true,  "(El conductor sutilmente sonríe de manera macabra, con un bulto en el asiento de copiloto)", LoadTextOption.EndWithArrow),
        new Dialog(35, "", true,  "(Te subes a la parte de atrás y tranquilamente cierras la puerta)", LoadTextOption.EndWithoutArrow),
        new Dialog(36, "image015", false,  "<size=30>¡¿Pero qué rayos?!</size>", LoadTextOption.EndWithArrow),
        new Dialog(37, "", true,  "(Unas manos te someten desde atrás)", LoadTextOption.EndWithArrow),
        new Dialog(38, "image016", false,  "(El vehículo se aleja en el horizonte camino al amanecer)", LoadTextOption.EndWithoutArrow),
        new Dialog(39, "", true,  "Le dije que era su <color=#ff0000><b>última</b></color> oportunidad – Dice el conductor.", LoadTextOption.EndWithArrow),
        new Dialog(40, "", true,  "(Mientras el vehículo se aleja más, se escucha una macabra risa en voz alta)", LoadTextOption.EndWithArrow),
        new Dialog(-1, "", true,  "<size=35><color=#ff0000>Fin del juego</color></size>", LoadTextOption.EndWithoutArrow)
    };
    }
}


