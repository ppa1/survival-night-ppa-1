﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayVideo : MonoBehaviour {

    public MovieTexture movie;

    public void Play(bool loop = true)
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        movie.loop = loop;
        movie.Play();
    }
}
