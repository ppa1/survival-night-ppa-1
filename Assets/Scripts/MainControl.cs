﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SurvivalNightStory;

public class MainControl : MonoBehaviour {
    public Animator ButtonYes;
    public Animator ButtonNo;
    public TextTyper MainTT;
    public PlayVideo MainVidTexture;
    private int counter = 0;
    private int yesOption;
    private int noOption;
    private bool buttonsSet;

    private void Awake()
    {
        TextTyperNext();
    }

    public void PressYes()
    {
        counter = yesOption;
        TextTyperNext();
    }

    public void PressNo()
    {
        counter = noOption;
        TextTyperNext();
    }

    public void TextTyperNext()
    {
        StartCoroutine(TextTyperNextCoroutine());
    }

    IEnumerator TextTyperNextCoroutine()
    {
        if (counter >= StoryScript.dialogs.Length || counter < 0)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            if (StoryScript.dialogs[counter].buttons)
            {
                yesOption = StoryScript.dialogs[counter].yes;
                noOption = StoryScript.dialogs[counter].no;
            }
            else if(buttonsSet)
            {
                yield return StartCoroutine(SetEnableButtons(false));
            }
            SetDisplay(StoryScript.dialogs[counter]);
            counter = StoryScript.dialogs[counter].next;
        }
    }

    void SetDisplay(Dialog dg)
    {
        if (dg.image != "")
        {
            MovieTexture spr = Resources.Load<MovieTexture>("Backgrounds/" + dg.image);
            if (spr == null)
                Debug.LogError("Expected Clip '" + dg.image + "' Not Found.");
            else
            {
                MainVidTexture.movie = spr;
                MainVidTexture.Play(dg.loop);
            }
        }
        if(dg.textYes!="")
            ButtonYes.GetComponentInChildren<Text>().text = dg.textYes;
        if (dg.textNo != "")
            ButtonNo.GetComponentInChildren<Text>().text = dg.textNo;
        MainTT.NextEnabled = true;
        MainTT.loadTextOption = dg.option;
        MainTT.ShowText(dg.message);
    }

    IEnumerator SetEnableButtons(bool value)
    {
        buttonsSet = value;
        ButtonYes.GetComponent<Button>().interactable = false;
        ButtonNo.GetComponent<Button>().interactable = false;
        while (MainTT.IsTypingText && value)
            yield return false;
        ButtonYes.SetBool("IsShown", value);
        ButtonNo.SetBool("IsShown", value);
        while (ButtonYes.GetCurrentAnimatorStateInfo(0).normalizedTime < 1 || ButtonYes.IsInTransition(0) ||
            ButtonNo.GetCurrentAnimatorStateInfo(0).normalizedTime < 1 || ButtonNo.IsInTransition(0))
            yield return false;
        ButtonYes.GetComponent<Button>().interactable = value;
        ButtonNo.GetComponent<Button>().interactable = value;
    }

    void ShowButtons()
    {
        StartCoroutine(SetEnableButtons(true));
    }
}
