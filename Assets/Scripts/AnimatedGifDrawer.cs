﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

[RequireComponent(typeof(UnityEngine.UI.Image))]
public class AnimatedGifDrawer : MonoBehaviour
{
    public float speed = 10;
    public bool loadGifsFromDatFile = true;
    public bool animate = true;
    private UnityEngine.UI.Image image;
    private static Dictionary<string, List<Sprite>> gifLibrary;
    private string CurrentGifPlayed;

    private string[] GetAllGifPaths()
    {
        string[] fileEntries = Directory.GetFiles(Application.dataPath + "/Gifs/");
        List<string> result = new List<string>();
        foreach (string fileName in fileEntries)
            if (Path.GetExtension(fileName).ToLower() == ".gif")
                result.Add(Path.GetFileName(fileName));
        return result.ToArray();
    }

    private void CreateGifLibrary()
    {
        gifLibrary = new Dictionary<string, List<Sprite>>();
        foreach (string gif in GetAllGifPaths())
        {
            var gifFrames = new List<Sprite>();
            var gifImage = Image.FromFile(@"Assets/Resources/Gifs/" + gif);
            var dimension = new FrameDimension(gifImage.FrameDimensionsList[0]);
            int frameCount = gifImage.GetFrameCount(dimension);
            for (int i = 0; i < frameCount; i++)
            {
                gifImage.SelectActiveFrame(dimension, i);
                var frame = new Bitmap(gifImage.Width, gifImage.Height);
                System.Drawing.Graphics.FromImage(frame).DrawImage(gifImage, Point.Empty);
                var frameTexture = new Texture2D(frame.Width, frame.Height);
                for (int x = 0; x < frame.Width; x++)
                    for (int y = 0; y < frame.Height; y++)
                    {
                        System.Drawing.Color sourceColor = frame.GetPixel(x, y);
                        frameTexture.SetPixel(-frame.Width + 1 + x, -y,
                            new Color32(sourceColor.R, sourceColor.G, sourceColor.B, sourceColor.A));
                    }
                frameTexture.Apply();
                Rect rect = new Rect(0, 0, frameTexture.width, frameTexture.height);
                gifFrames.Add(Sprite.Create(frameTexture, rect, new Vector2(0.5f, 0.5f)));
            }
            gifLibrary.Add(gif.ToLower(),gifFrames);
        }
        SaveGifLibrary();
    }

    private void SaveGifLibrary()
    {
        BinaryFormatter binFor = new BinaryFormatter();
        FileStream file = File.Open(Application.dataPath
            + "/Gifs.dat", FileMode.OpenOrCreate);
        try
        {
            var saveFile = new GifLibrary();
            foreach (var key in gifLibrary.Keys)
            {
                var images = new List<GifLibrary.SavedImage>();
                foreach (var s in gifLibrary[key])
                {
                    images.Add(new GifLibrary.SavedImage(s.texture.EncodeToPNG(),
                        new int[] { s.texture.width, s.texture.height }));
                }
                saveFile.gifLibrary.Add(key, images.ToArray());
            }
            binFor.Serialize(file, saveFile);
        }
        catch (Exception e)
        {
            Debug.LogError("Error Saving Gif Library: " + e.Message);
        }
        finally
        {
            file.Close();
        }
    }

    private void LoadGifLibrary()
    {
        if (File.Exists(Application.dataPath
            + "/Gifs.dat"))
        {
            BinaryFormatter binFor = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath
                + "/Gifs.dat", FileMode.Open);
            try
            {
                var saveFile = (GifLibrary)binFor.Deserialize(file);
                gifLibrary = new Dictionary<string, List<Sprite>>();
                foreach (var key in saveFile.gifLibrary.Keys)
                {
                    var gif = new List<Sprite>();
                    foreach (var im in saveFile.gifLibrary[key])
                    {
                        var frameTexture = new Texture2D(im.dimentions[0], im.dimentions[1]);
                        frameTexture.LoadImage(im.image);
                        frameTexture.Apply();
                        Rect rect = new Rect(0, 0, frameTexture.width, frameTexture.height);
                        gif.Add(Sprite.Create(frameTexture, rect, new Vector2(0.5f, 0.5f)));
                    }
                    gifLibrary.Add(key, gif);
                }
            }
            catch(Exception e)
            {
                Debug.LogError("Error Loading Gif Library: " + e.Message + "\n"+ e.StackTrace);
                CreateGifLibrary();
            }
            finally
            {
                file.Close();
            }
        }
        else
        {
            CreateGifLibrary();
        }
    }

    private void Awake()
    {
        if (gifLibrary == null)
        {
            if (loadGifsFromDatFile)
                LoadGifLibrary();
            else
                CreateGifLibrary();
        }
        image = GetComponent<UnityEngine.UI.Image>();
    }

    public void SetNewGif(string source)
    {
        CurrentGifPlayed = source.ToLower();
    }

    void Update()
    {
        if (gifLibrary != null && CurrentGifPlayed != "")
        {
            int frameIndex = (int)(Time.time * speed) % gifLibrary[CurrentGifPlayed].Count;
            image.sprite = gifLibrary[CurrentGifPlayed][frameIndex];
        }
    }
}

[Serializable]
class GifLibrary
{
    public Dictionary<string, SavedImage[]> gifLibrary = new Dictionary<string, SavedImage[]>();
    [Serializable]
    public class SavedImage
    {
        public byte[] image;
        public int[] dimentions;

        public SavedImage(byte[] bytes, int[] dim)
        {
            this.image = bytes;
            this.dimentions = dim;
        }
    }
}